# 事务管理器用法

## 事务管理器介绍

事务管理器是cad .net二次开发过程绕不过去的一个部分，只要是涉及到读写cad数据的地方几乎都推荐在事务里完成。利用事务管理器可以自动的在退出事务的时候执行释放对象等操作，防止程序员不能释放对象，造成cad崩溃。

但是，在日常的使用中，会发现每次开启事务，然后完成的都是差不多的任务，然后每次都要调用commit()函数，每次都要获取到符号表，每次要写模式，读模式等提权降级操作，但是这些操作其实都可以自动完成的，因此NFox内裤提供事务管理器类来完成本来需要手工完成的工作，让用户可以更方便的处理事务内的程序。

用事务管理器类可以完成：

- 原生cad提供的事务管理器的全部操作
- 方便的符号表操作
- 方便的添加图元操作
- 方便的基础属性操作
- 方便的对象获取操作
- 方便的块相关操作
- 方便的字典操作

事务管理器类的类名为：DBTransaction。开启事务管理器的写法为：

```c#
using (DBTransaction tr = new DBTransaction())
{
  ....
}
```

## 原生的事务管理器操作

关于cad提供的原生事务管理器的操作不是本文档的重点，因为那操作起来麻烦，不够集中的将需要在事务内的操作做统一管理。

## 符号表操作

NFox内裤的符号表其实是一个符号表的泛型类，直接将符号表和符号表记录包装为一个整体。不用担心，在实际使用的过程中，你几乎不会关心符号表的构成原理。

NFox内裤里采用如下的符号来表示9大符号表。

|    符号表名    | 符号表含义 |
| :------------: | :--------: |
|   BlockTable   |    块表    |
|   LayerTable   |   图层表   |
| DimStyleTable  | 标注样式表 |
| LinetypeTable  |   线型表   |
|  RegAppTable   | 应用程序表 |
| TextStyleTable | 字体样式表 |
|    UcsTable    |  坐标系表  |
| ViewportTable  |   视口表   |
|   ViewTable    |   视图表   |

然后怎么使用呢？使用符号表一共分几步呢？

```c#
using (DBTransaction tr = new DBTransaction()) 
{ // 第一步，开启事务
  	var layerTable = tr.LayerTable;
  // 第二步，获取图层表
} // 事务结束，会自动的提交
```

上面是一个获取层表的例子，其他的符号表都是一样的写法，因为这些符号表都是事务管理器的属性。那么获取到符号表之后能做些什么？

- 向符号表里添加元素

  ```c#
  using (DBTransaction tr = new DBTransaction()) 
  { // 第一步，开启事务
    	var layerTable = tr.LayerTable;
    // 第二步，获取图层表
    	layerTable.Add("1");// 返回值为ObjectId
    // 第三步，向层表里添加一个元素，也就是新建一个图层。
  } // 事务结束，会自动的提交
  ```

  每个符号表都有Add函数，而且提供了不止一个重载函数。

- 获取符号表里的元素

  想要获取符号表内的某个元素非常的简单：

  ```c#
  using (DBTransaction tr = new DBTransaction()) 
  { // 第一步，开启事务
    	var layerTable = tr.LayerTable;
    // 第二步，获取图层表
    	layerTable.Add("1"); // 返回值为ObjectId
    // 第三步，向层表里添加一个元素，也就是新建一个图层。
    	ObjectId id = layerTable["1"];
    // 第四步，获取图层“1”的id。
  } // 事务结束，会自动的提交
  ```

  每个符号表都提供了索引形式的获取元素id的写法。

- 线型表

  ```c#
  // 两种方式
  // 第一种，直接调用tr.LinetypeTable.Add("hah")函数，然后对返回值ObjectId做具体的操作。
  // 第二种，直接在Action委托里把相关的操作完成。
  tr.LinetypeTable.Add(
                      "hah",
                      ltt => 
                      {
                          ltt.AsciiDescription = "虚线";
                          ltt.PatternLength = 0.95; //线型的总长度
                          ltt.NumDashes = 4; //组成线型的笔画数目
                          ltt.SetDashLengthAt(0, 0.5); //0.5个单位的划线
                          ltt.SetDashLengthAt(1, -0.25); //0.25个单位的空格
                          ltt.SetDashLengthAt(2, 0); // 一个点
                          ltt.SetDashLengthAt(3, -0.25); //0.25个单位的空格
                      });
  // 这段代码同时演示了NFox内裤关于符号表的public ObjectId Add(string name, Action<TRecord> action)这个函数的用法。
  // 或者直接调用：
  tr.LinetypeTable.Add("hah", "虚线",0.95,new double[]{0.5,-0.25,0,-0.25});
  // 获取线型表
  tr.LinetypeTable["hah"];
  ```

  其他符号表就真的大同小异了。如果内裤没有提供的Add函数的重载，那么Action委托可以完成你想完成的所有事情。

## 事务管理器添加图元

一般的情况下，添加图元的操作都是要在事务里完成。目前大部分的添加图元的自定义函数都是Editor对象的扩展函数。但是实际上添加图元的本质是读写图形数据库，具体的手段是对块表里的块表记录的读写。而实际的操作其实都是在事务里完成，所以符合cad操作规则的写法其实应该是事务作为一系列操作的主体来进行。因此NFox内裤封装的事务管理器类里提供了添加图元的系列函数。

对于这个添加图元的操作，一共分为如下几步：

1. 创建图元对象，可以在事务外创建，也可以在事务内创建。
2. 打开要添加图元的块表记录，在事务内打开。
3. 添加图元到块表记录

下面看示例：

- 添加图元到当前空间

	```c#
	using (DBTransaction tr = new DBTransaction()) //开启事务管理器
	{
	    Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0)); //定义一个直线
	    var btr = tr.OpenCurrentSpace(); //打开当前的空间的块表记录
	    tr.AddEntity(btr, line1); // 将直线添加到当前控件的块表记录
	}
	```
	
- 添加图元到模型/图纸空间
	
	```c#
	using (DBTransaction tr = new DBTransaction())
	{
	    Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0)); //定义一个直线
	    var btr = tr.OpenModelSpace(); //打开模型空间的块表记录
	    //var btr = tr.OpenPaperSpace(); //打开模型空间的块表记录
	    tr.AddEntity(btr, line1); // 将直线添加到当前控件的块表记录
	}
	```
	
- 添加图元到块表
    ```c#
     using (DBTransaction tr = new DBTransaction()) //开启事务管理器
    {
        Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0)); //定义一个直线
        var btr = tr.BlockTable.Add("test"); //定义一个块表记录
        tr.AddEntity(btr, line1); // 将直线添加到当前控件的块表记录
    }
    ```
    那么大家猜一猜，这个添加到块表是实现了一种什么样的功能。
	
- 块表
	
	块表这里需要特殊的说明一下：
	
	比如说添加一个块，用如下代码：
	
	`tr.BlockTable.Add(blockName, btr => table.Trans.AddEntity(btr, ents));`
	
	这里的blockName就是块名，ents就是图元列表。但是这种方式虽然可以更细粒度的控制定义的块，但是使用起来还是不方便，因此内裤提供了一些封装的函数用来定义块，插入块参照，比如：
	
	`tr.AddBlock(name,ents); //用于定义块，提供了重载函数来满足不同的需求`
	
	`tr.InsertBlock(point,objectid); // 用于插入块参照，提供了重载函数来满足不同的需求`
	
	当然还可以用Add函数来定义块：`tr.BlockTable.Add(name,ents);`
	
	如果熟悉内裤的使用，建议还是用Add函数的方式来定义块，因为上面的所有AddBlock函数其实都是对Add函数的二次封装，只不过名字更易懂而已。

- 其他函数的介绍
	
	`OpenBlockTableRecord`  函数，用于打开块表，4个重载函数，可以控制打开模式。
	
	`AppendAttribToBlock`  函数，添加属性到块参照，有重载函数
	
	`tr.BlockTable.GetRecord()` 函数，可以获取到块表的块表记录，同理层表等符号表也有同样的函数。
	
	`tr.BlockTable.GetRecordFrom()` 函数，可以从文件拷贝块表记录，同理层表等符号表也有同样的函数。
	
	`tr.BlockTable.GetBlockFrom()` 函数，从文件拷贝块定义，同理层表等符号表也有同样用途的函数。

## 基础属性操作

事务管理器类提供了`Document`、 `Editor` 、`Database`三个属性来在事务内部处理相关事项。

同时还提供了关于字典的相关属性。

## 对象获取操作

提供了6个`GetObject`函数的重载来根据ObjectId来获取到对象。用户可以根据需要自行选择。

## 块相关操作

除了上面符号表里提到的块定义和插入块参照函数外，内裤还提供了其他的块相关操作函数。

- 添加属性到块参照

  提供了4个 `AppendAttribToBlock` 函数的重载来完成将属性添加到块参照的功能。

- 动态添加块参照

  提供了4个 `InsertBlockRef` 函数的重载来完成动态添加块参照的功能。

- 裁剪块参照

  提供了2个`ClipBlockRef`函数的重载来完成裁剪块参照的功能，其中一个是多边形裁剪，一个是矩形裁剪。

## 字典操作

- 扩展字典

  `SetXRecord` 保存扩展数据到字典

  `GetXRecord ` 获取扩展数据

- 对象字典

  `SetToDictionary` 保存数据到字典

  `GetFromDictionary` 从字典获取数据

  `GetSubDictionary` 获取子对象字典

